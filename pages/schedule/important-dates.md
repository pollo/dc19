---
name: Important Dates
---

Important Dates
===============

| **July**              |                                                                      |
| Sunday, 14 July       | First day of DebCamp                                                 |
| Monday, 15 July       | Second day of DebCamp                                                |
| Tuesday, 16 July      | Third day of DebCamp                                                 |
| Wednesday, 17 July    | Fourth day of DebCamp                                                |
| Thursday, 18 July     | Fifth day of DebCamp                                                 |
| Friday, 19 July       | Sixth day of DebCamp                                                 |
| Saturday, 20 July     | Open Day; afternoon: job fair; Arrival day for DebConf, Set-up       |
| Sunday, 21 July       | First day of DebConf: morning: Opening Ceremony                      |
| Monday, 22 July       | Second day of DebConf: Cheese and Wine Party                         |
| Tuesday, 23 July      | Third day of DebConf                                                 |
| Wednesday, 24 July    | Day trip                                                             |
| Thursday, 25 July     | Fifth day of DebConf: Conference dinner                              |
| Friday, 26 July       | Sixth day of DebConf                                                 |
| Saturday, 27 July     | Last day of DebConf: Closing ceremony; Teardown                      |
| Sunday, 28 July       | Morning: Handicraft fair; lunch with brazilian barbecue              |
| Monday, 29 July       | Departure day                                                        |
